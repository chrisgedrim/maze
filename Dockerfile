FROM node:jessie

RUN mkdir -p /home/node/app
RUN chown -R node:node /home/node/app

WORKDIR /home/node/app
USER node

COPY package*.json ./
COPY index.js ./
COPY maze3DBreadth.js ./
COPY maps ./maps/

RUN npm ci

CMD ["node", "index"]
