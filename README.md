# Maze Solving Challenge

This implementation of a maze solving program has been written in Javascript for execution with Node JS version 10 (or greater.)

It _should_ work with lesser versions of Node JS, however this has not been tested.

## Running the test code

The code can be executed in one of two ways:

Manually

```
$> npm install
$> node index
```

_or_

With Docker

```
docker build -t maze .
docker run -t maze
```

### Running custom scenarios

Custom mazes can be tested by creating a Javascript file, importing the maze class, and passing it a maze specification:

```javascript
const Maze = require('./maze');

const map2D = [
  ['X', 'O', 'X', 'X'],
  ['X', 'O', 'X', 'X'],
  ['X', 'O', 'S', 'X'],
  ['X', 'X', 'X', 'X']
];

const map3D = [
  [
    ['X', 'O', 'X', 'X'],
    ['O', 'O', 'O', 'X'],
    ['X', 'X', 'O', 'X'],
    ['X', 'X', 'X', 'X']
  ],
  [
    ['X', 'X', 'X', 'X'],
    ['X', 'O', 'O', 'X'],
    ['X', 'O', 'O', 'X'],
    ['X', 'X', 'X', 'X']
  ],
  [
    ['X', 'X', 'X', 'X'],
    ['X', 'O', 'X', 'X'],
    ['X', 'O', 'S', 'X'],
    ['X', 'X', 'X', 'X']
  ]
];

new Maze(map2D).solve();

const solveAllExits = true;
const showShortestPath = true;
const showAllPaths = true;

new Maze(map3D, solveAllExits, showShortestPath, showAllPaths).solve();
```

### Program output

The program will print information about the current maze, it's solution(s), it's maps, and some metrics, to stdout. Chalk<sup>2</sup> is used while printing, so a TTY which supports colors is needed for map output.

The example code above outputs the following:

<div style="background:black;color:white;font-family:monospace;">
<span style="color:green;">Level 0</span><br />
<br />
<div style="color:blue;line-height:16px;">
<span style="background:blue;">██<span style="color:green;background:green;">██</span>████</span><br />
<span style="background:blue;">██<span style="color:green;background:green;">██</span>████</span><br />
<span style="background:blue;">██<span style="color:green;background:green;">██</span><span style="color:red;background:red;">██</span>██<br />
<span style="background:blue;">████████</span><br />
</div>
<br />
<div style="line-height:16px;">
┌─────────────────┬──────────┐<br />
│&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(index)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;│&nbsp;&nbsp;Values&nbsp;&nbsp;│<br />
├─────────────────┼──────────┤<br />
│ Shortest path&nbsp;&nbsp; │ &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:yellow;">3</span>&nbsp;&nbsp;&nbsp; │<br />
│ Steps taken&nbsp;&nbsp;&nbsp;&nbsp; │ &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:yellow;">4</span>&nbsp;&nbsp;&nbsp; │<br />
│ Elapsed seconds │ <span style="color:yellow;">0.000733</span> │<br />
└─────────────────┴──────────┘<br />
<br />
<span style="color:green;">Doors</span><br />
<br />
┌─────────┬───┬───┬───┬───┐<br />
│ (index) │ x │ y │ z │ d │<br />
├─────────┼───┼───┼───┼───┤<br />
│ &nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp; │ <span style="color:yellow;">1</span> │ <span style="color:yellow;">2</span> │ <span style="color:yellow;">1</span> │ <span style="color:yellow;">2</span> │<br />
│ &nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp; │ <span style="color:yellow;">1</span> │ <span style="color:yellow;">1</span> │ <span style="color:yellow;">0</span> │ <span style="color:yellow;">4</span> │<br />
└─────────┴───┴───┴───┴───┘<br />
<br />
<span style="color:green;">Other exits were at</span><br />
<br />
┌─────────┬───┬───┬───┬───┐<br />
│ (index) │ x │ y │ z │ d │<br />
├─────────┼───┼───┼───┼───┤<br />
│ &nbsp;&nbsp;&nbsp;0&nbsp;&nbsp;&nbsp; │ <span style="color:yellow;">1</span> │ <span style="color:yellow;">0</span> │ <span style="color:yellow;">0</span> │ <span style="color:yellow;">5</span> │<br />
└─────────┴───┴───┴───┴───┘<br />
</div>
<br />
<span style="color:green;">Level 0</span><br />
<br />
<div style="color:blue;line-height:16px;">
<span style="background:blue;">██<span style="color:orange;background:orange;">██</span>████</span><br />
<span style="background:blue;"><span style="color:magenta;background:magenta;">██</span><span style="color:orange;background:magenta;">░░</span><span style="color:green;background:green;">██</span>██</span><br />
<span style="background:blue;">████<span style="color:green;background:green;">██</span>██</span><br />
<span style="background:blue;">████████</span><br />
</div>
<br />
<span style="color:green">Level 1</span><br />
<br />
<div style="color:blue;line-height:16px;">
<span style="background:blue;">████████</span><br />
<span style="background:blue;">██<span style="color:orange;background:magenta;">░░</span><span style="color:green;background:green;">██</span>██</span><br />
<span style="background:blue;">██<span style="color:orange;background:magenta;">░░</span>████</span><br />
<span style="background:blue;">████████</span><br />
</div>
<br />
<span style="color:green">Level 2</span><br />
<br />
<div style="color:blue;line-height:16px;">
<span style="background:blue;">████████</span><br />
<span style="background:blue;">██<span style="color:green;background:green">██</span>████</span><br />
<span style="background:blue;">██<span style="color:orange;background:magenta;">░░</span><span style="color:red;background:red;">██</span>██</span><br />
<span style="background:blue;">████████</span><br />
</div>
<div style="line-height:16px;">
<br>
┌─────────────────┬──────────┐<br />
│ &nbsp;&nbsp;&nbsp;&nbsp;(index)&nbsp;&nbsp;&nbsp;&nbsp; │ &nbsp;Values&nbsp; │<br />
├─────────────────┼──────────┤<br />
│ Shortest path&nbsp;&nbsp; │ &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:yellow">5</span>&nbsp;&nbsp;&nbsp; │<br />
│ Steps taken&nbsp;&nbsp;&nbsp;&nbsp; │ &nbsp;&nbsp;&nbsp;<span style="color:yellow">11</span>&nbsp;&nbsp;&nbsp; │<br />
│ Elapsed seconds │ <span style="color:yellow">0.003237</span> │<br />
└─────────────────┴──────────┘<br />
</div>
</div>

## Developer notes

- The test code runs through a series of scenarios
  - The example maze given in the specification
  - A simple, hand-coded, 3D maze with 3 floors, based on the example maze given in the specification
  - A generated<sup>1</sup> 2D maze
  - A generated<sup>1</sup> 2D maze with 5 exits
  - A generated<sup>1</sup> 2D maze with 5 exits and the shortest path marked
  - A generated<sup>1</sup> 3D maze with 5 exits and the shortest path marked
  - A generated<sup>1</sup> 3D maze with 3floors, 5 exits, and all paths marked
  - A large generated<sup>1</sup> 3D maze with 3 floors, 5 exits, and all paths marked

This is an imperfect solution:

- For three dimesional mazes the logic attempts to find 'doors' between floors with the fewest number of steps needed from the start point, each floor is then solved in turn. This can lead to suboptimal paths being discovered (it may be shorter to move across a floor before moving up/down a floor.) The logic was originally written to look for the absolute shortest route but was refactored due to the prohibitively long execution time.
- Occasionally, and I've not had chance for a full investigation, routes fail to get calculated correctly (an error showing which step was being processed is output.)
- Due to the underlying architecture of Node JS a lower-level language (Python, Java, C, etc) would have been more suitable choice for this challenge.

## References

<sup>1</sup> Mazes are generated based on labyrinth images generated at https://donjon.bin.sh/fantasy/dungeon/labyrinth.cgi
