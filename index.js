const { createReadStream, readdir } = require('fs');
const { PNG } = require('pngjs');
const Maze = require('./maze');
const { green } = require('chalk');
const { join, extname } = require('path');
let basePath = 'maps/small';

console.log(green('\nProcessing simple maze\n'));
new Maze([
  ['X', 'O', 'X', 'X', 'X', 'X'],
  ['X', 'O', 'X', 'O', 'O', 'X'],
  ['X', 'O', 'X', 'O', 'X', 'X'],
  ['X', 'O', 'O', 'O', 'S', 'X'],
  ['X', 'X', 'X', 'X', 'X', 'X']
]).solve();

console.log(green('\nProcessing simple 3D maze\n'));
new Maze([
  [
    ['X', 'X', 'X', 'X', 'X', 'X'],
    ['X', 'X', 'X', 'X', 'X', 'X'],
    ['X', 'X', 'X', 'X', 'O', 'X'],
    ['X', 'O', 'O', 'O', 'O', 'X'],
    ['X', 'X', 'X', 'X', 'X', 'X']
  ],
  [
    ['X', 'O', 'X', 'X', 'X', 'X'],
    ['X', 'O', 'X', 'O', 'O', 'X'],
    ['X', 'O', 'X', 'O', 'X', 'X'],
    ['X', 'O', 'O', 'O', 'S', 'X'],
    ['X', 'X', 'X', 'X', 'X', 'X']
  ],
  [
    ['X', 'X', 'X', 'X', 'X', 'X'],
    ['X', 'O', 'O', 'O', 'O', 'X'],
    ['X', 'O', 'X', 'O', 'X', 'X'],
    ['X', 'O', 'O', 'O', 'X', 'X'],
    ['X', 'X', 'X', 'X', 'X', 'X']
  ]
]).solve();

buildMap = png => {
  const generatedMap = [];

  // Tiles are 15x15 with a shared 1px border, effectively making each tile 13x13
  for (let y = 7; y < png.height; y += 15) {
    const xArr = [];
    for (let x = 7; x < png.width; x += 15) {
      const idx = (png.width * y + x) << 2;

      xArr.push(png.data[idx] ? 'O' : 'X');
    }
    generatedMap.push(xArr);
  }

  return generatedMap;
};

addStartToMap = generatedMap => {
  // Add a start point
  const { row, col } = getStartRowCol(generatedMap);
  generatedMap[row][col] = 'S';

  return generatedMap;
};

addExitsToMap = (generatedMap, exits = 1) => {
  // Add an end point(s)
  for (let i = 0; i < exits; i++) {
    const { row, col } = getExitRowCol(generatedMap);
    generatedMap[row][col] = 'O';
  }

  return generatedMap;
};

getPngData = async file =>
  new Promise(resolve => {
    createReadStream(join(__dirname, basePath, file))
      .pipe(new PNG())
      .on('parsed', function() {
        resolve(this);
      });
  });

generateMap = async (
  levels = 1,
  exits = 1,
  solveAllExits = false,
  markShortestPath = false,
  markAllPaths = false
) => {
  await new Promise(resolve => {
    readdir(
      join(__dirname, basePath),
      { withFileTypes: true },
      async (err, dirents) => {
        if (err) {
          console.error('Error reading maps', err);
        }

        const files = dirents
          .filter(dirent => dirent.isFile())
          .filter(dirent => extname(dirent.name).toLowerCase() === '.png')
          .map(dirent => dirent.name);

        const generatedMap = [];
        let map = files[Math.floor(Math.random() * Math.floor(files.length))];
        const maps = [map];
        let png = await getPngData(map);
        let mapData = buildMap(png);

        if (levels === 1) {
          mapData = addStartToMap(mapData);
          mapData = addExitsToMap(mapData, exits);
          generatedMap.push(mapData);

          console.log(green('Map in use'));
          console.log(map);
        } else {
          generatedMap.push(mapData);

          for (let i = 1; i < levels; i++) {
            let map =
              files[Math.floor(Math.random() * Math.floor(files.length))];
            maps.push(map);
            let png = await getPngData(map);
            generatedMap.push(buildMap(png));
          }

          for (let i = 0; i < exits; i++) {
            const floor = Math.floor(
              Math.random() * Math.floor(generatedMap.length)
            );
            generatedMap[floor] = addExitsToMap(generatedMap[floor]);
          }

          const floor = Math.floor(
            Math.random() * Math.floor(generatedMap.length)
          );

          generatedMap[floor] = addStartToMap(generatedMap[floor]);

          console.log(green('Maps in use'));
          console.table(maps);
        }

        new Maze(
          generatedMap,
          solveAllExits,
          markShortestPath,
          markAllPaths
        ).solve();
        resolve();
      }
    );
  });
};

getStartRowCol = map => {
  const ly = map.length;
  const lx = map[0].length;

  while (true) {
    const row = Math.floor(1 + Math.random() * (ly - 1 - 1));
    const col = Math.floor(1 + Math.random() * (lx - 1 - 1));

    if (hasValidMove(col, row, map)) {
      return { row, col };
    }
  }
};

getExitRowCol = map => {
  const ly = map.length;
  const lx = map[0].length;

  while (true) {
    const row = Math.floor(Math.random() * Math.floor(ly - 1));
    const col =
      row === 0 || row === ly - 1
        ? Math.floor(Math.random() * Math.floor(lx))
        : Math.floor(Math.random() * Math.floor(2)) * (lx - 1);

    if (hasValidMove(col, row, map)) {
      return { row, col };
    }
  }
};

hasValidMove = (x, y, map) => {
  // Check cell isn't already an exit
  if (map[y][x] === 'O') {
    return false;
  }

  const xMoves = [-1, 0, 0, 1];
  const yMoves = [0, -1, 1, 0];

  for (let i = 0; i < 4; i++) {
    const xMove = x + xMoves[i];
    const yMove = y + yMoves[i];

    if (isValidMove(xMove, yMove, map)) {
      return true;
    }
  }

  return false;
};

isValidMove = (x, y, map) => {
  if (x > map[0].length - 1 || x < 0) {
    return false;
  }

  if (y > map.length - 1 || y < 0) {
    return false;
  }

  const cell = map[y][x];

  if (cell === 'X') {
    return false;
  }

  return true;
};

runGenerated = async () => {
  console.log(green('\nProcessing simple generated maze\n'));
  await generateMap();
  console.log(green('\nProcessing simple generated maze with 5 exits\n'));
  await generateMap(1, 5, true);
  console.log(
    green(
      '\nProcessing simple generated maze with 5 exits, marking the shortest path\n'
    )
  );
  await generateMap(1, 5, true, true);
  console.log(
    green(
      '\nProcessing 3D generated maze with 5 exits, marking the shortest path\n'
    )
  );
  await generateMap(3, 5, true, true);
  console.log(
    green(
      '\nProcessing 3D generated maze with 5 exits, marking the all paths\n'
    )
  );
  await generateMap(3, 5, true, true, true);
  basePath = 'maps';
  console.log(
    green(
      '\nProcessing a large 3D generated maze with 5 exits, marking the shortest path\n'
    )
  );
  await generateMap(3, 5, true, true);
};

runGenerated();
