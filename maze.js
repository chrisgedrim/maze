const chalk = require('chalk');
const { red, white, blue, green, magenta, hex } = chalk;
const block = String.fromCharCode(0x2588) + String.fromCharCode(0x2588);
const sharedBlock = String.fromCharCode(0x2591) + String.fromCharCode(0x2591);
const door = String.fromCharCode(0x254b) + String.fromCharCode(0x254b);
const orange = '#ff8800';

class Maze {
  constructor(
    source,
    solveAllExits = false,
    showShortestPath = false,
    showAllPaths = false
  ) {
    this.source = source;
    if (!Array.isArray(this.source[0][0])) {
      this.source = [this.source];
    }
    this.maxX = this.source[0][0].length - 1;
    this.maxY = this.source[0].length - 1;
    this.maxZ = this.source.length - 1;
    this.solveAllExits = solveAllExits;
    this.showShortestPath = showShortestPath;
    this.showAllPaths = showAllPaths;
    this.exits = [];
    this.totalExits = 0;
    this.history = [].concat(this.source).map((l, z) =>
      l.map((m, y) =>
        m.map((n, x) => {
          switch (n) {
            case 'X':
              return {
                wall: true
              };
            case 'O':
              if (this.isAtEdge(x, y)) {
                this.totalExits++;
              }
              return {
                wall: false,
                visited: false
              };
            case 'S':
              this.start = { x, y, z, d: 0 };
              return {
                wall: false,
                visited: false
              };
          }
        })
      )
    );
  }

  findDoors() {
    const { z } = this.start;
    let queue = [this.start];
    const doors = [];
    const xMoves = [1, -1, 0, 0];
    const yMoves = [0, 0, 1, -1];
    const history = [...this.history];

    // Find routes down
    for (let i = z; i > 0; i--) {
      while (queue.length) {
        const { x, y, d } = queue.shift();

        if (this.isValidMove(x, y, i - 1, history)) {
          history[i - 1][y][x].visited = true;
          this.history[i - 1][y][x].door = true;
          doors.push({ x, y, z: i - 1, d: d + 1 });
          queue = [{ x, y, z: i - 1, d: d + 1 }];
          break;
        } else {
          for (let j = 0; j < 4; j++) {
            const xMove = x + xMoves[j];
            const yMove = y + yMoves[j];
            if (this.isValidMove(xMove, yMove, i, history)) {
              history[i][y][x].visited = true;
              queue.push({ x: xMove, y: yMove, z: i, d: d + 1 });
            }
          }
        }
      }
    }

    queue = [this.start];

    // Find routes up
    for (let i = z; i < this.source.length - 1; i++) {
      while (queue.length) {
        const { x, y, d } = queue.shift();

        if (this.isValidMove(x, y, i + 1, history)) {
          history[i + 1][y][x].visited = true;
          this.history[i + 1][y][x].door = true;
          doors.push({ x, y, z: i + 1, d: d + 1 });
          queue = [{ x, y, z: i + 1, d: d + 1 }];
          break;
        } else {
          for (let j = 0; j < 4; j++) {
            const xMove = x + xMoves[j];
            const yMove = y + yMoves[j];
            if (this.isValidMove(xMove, yMove, i, history)) {
              history[z][y][x].visited = true;
              queue.push({ x: xMove, y: yMove, z: i, d: d + 1 });
            }
          }
        }
      }
    }

    if (doors.length) {
      console.log(green('Doors'));
      console.table(doors);
    }

    return doors;
  }

  solve() {
    const startTime = process.hrtime();
    const queue = [this.start];
    const doors = this.findDoors();
    const moves = [];
    let minD = Infinity;
    const xMoves = [-1, 1, 0, 0];
    const yMoves = [0, 0, -1, 1];

    while (queue.length) {
      queue.sort((a, b) => {
        if (a.d > b.d) {
          return 1;
        }

        if (a.d < b.d) {
          return -1;
        }

        return 0;
      });
      const { x, y, z, d } = queue.shift();
      moves.push({ x, y, z, d });
      this.history[z][y][x] = {
        ...this.history[z][y][x],
        visited: true,
        dist: d
      };

      if (this.isAtEdge(x, y)) {
        this.exits.push({ x, y, z, d });
        minD = Math.min(d, minD);
      }

      for (let i = 0; i < 4; i++) {
        const xMove = x + xMoves[i];
        const yMove = y + yMoves[i];

        if (this.isValidMove(xMove, yMove, z)) {
          queue.push({ x: xMove, y: yMove, z, d: d + 1 });
        }
      }

      if (!queue.length && doors.length) {
        queue.push(doors.pop());
      }
    }

    const metrics = {};

    if (minD === Infinity) {
      console.log(red('Maze could not be solved'));
    } else {
      metrics['Shortest path'] = minD;
      metrics['Steps taken'] = this.history
        .map(z =>
          z
            .map(y => y.filter(x => x.visited).reduce(p => p + 1, 0))
            .reduce((p, c) => p + c, 0)
        )
        .reduce((p, c) => p + c, 0);

      this.dedupeExits();
      this.printSolutions();

      try {
        this.buildPaths();
      } catch (err) {}
    }

    const elapsedTime = process.hrtime(startTime);
    metrics['Elapsed seconds'] = parseFloat(
      Number((elapsedTime[0] * 1000 + elapsedTime[1] / 1000000) / 1000).toFixed(
        6
      )
    );

    this.printMaps();

    console.table(metrics);
  }

  dedupeExits() {
    this.exits = this.exits.reduce((u, i) => {
      const found = u.findIndex(
        el => el.x === i.x && el.y === i.y && el.d === i.d
      );
      if (found === -1) {
        return [...u, i];
      }
      return u;
    }, []);

    this.exits.sort((a, b) => (a.d > b.d ? 1 : a.d < b.d ? -1 : 0));
  }

  buildPaths() {
    if (!this.showShortestPath) {
      return;
    }

    const exits = [...this.exits];
    let exit = exits.shift();

    this.markShortestPath(exit);

    if (!this.showAllPaths) {
      return;
    }

    while (exits.length) {
      exit = exits.shift();
      this.markShortestPath(exit, false);
    }
  }

  printSolutions() {
    if (this.exits.length > 1) {
      console.log(green('\nOther exits were at\n'));
      console.table(this.exits.slice(1));
    }
  }

  printMaps() {
    this.source.forEach((z, zIndex) => {
      console.log(green(`\nLevel ${zIndex}\n`));
      z.map((y, yIndex) => {
        console.log(
          y
            .map((x, xIndex) => {
              switch (x) {
                case 'X':
                  return blue.bgBlue(block);
                case 'O':
                  if (this.history[zIndex][yIndex][xIndex].door) {
                    if (this.history[zIndex][yIndex][xIndex].altPath) {
                      return chalk.hex(orange).bgMagenta(door);
                    } else if (this.history[zIndex][yIndex][xIndex].path) {
                      return white.bgMagenta(door);
                    }

                    return green.bgWhite(door);
                  }
                  if (this.history[zIndex][yIndex][xIndex].path) {
                    if (this.history[zIndex][yIndex][xIndex].altPath) {
                      return chalk.hex(orange).bgMagenta(sharedBlock);
                    }

                    return magenta.bgMagenta(block);
                  }
                  if (this.history[zIndex][yIndex][xIndex].altPath) {
                    return chalk.hex(orange).bgHex(orange)(block);
                  }
                  if (this.history[zIndex][yIndex][xIndex].visited) {
                    return green.bgGreen(block);
                  }

                  return white.bgWhite(block);
                default:
                  return red.bgRed(block);
              }
            })
            .join('')
        );
      });
    });
  }

  markShortestPath(current, isShortest = true) {
    const route = this.getShortestRoute(current);

    route.forEach(
      ({ x, y, z }) =>
        (this.history[z][y][x][isShortest ? 'path' : 'altPath'] = true)
    );
  }

  getShortestRoute(current) {
    let route = [current];
    const xMoves = [-1, 0, 0, 1, 0, 0];
    const yMoves = [0, -1, 1, 0, 0, 0];
    const zMoves = [0, 0, 0, 0, -1, 1];
    let complete = false;

    while (!complete) {
      const { x, y, z, d } = current;

      if (this.source[z][y][x] === 'S') {
        break;
      }

      const moves = [];
      for (let i = 0; i < 6; i++) {
        const xMove = x + xMoves[i];
        const yMove = y + yMoves[i];
        const zMove = z + zMoves[i];

        if (this.isNextMove(xMove, yMove, zMove, d)) {
          moves.push({ x: xMove, y: yMove, z: zMove, d: d - 1 });
        }
      }

      if (!moves.length) {
        console.error('No moves found from');
        console.table(current);
        return false;
      } else if ((moves.length = 1)) {
        route.push(moves[0]);
        current = moves[0];
      } else {
        for (let i = 0; i < moves.length; i++) {
          const childRoute = this.markShortestPath(moves[i]);
          if (childRoute !== false) {
            route = route.concat(childRoute);
            complete = true;
            break;
          }
        }
      }
    }

    return route;
  }

  isNextMove(x, y, z, d) {
    if (
      this.history[z] &&
      this.history[z][y] &&
      this.history[z][y][x] &&
      this.history[z][y][x].visited &&
      this.history[z][y][x].dist === d - 1
    ) {
      return true;
    }

    return false;
  }

  isAtEdge(x, y) {
    return x === 0 || x === this.maxX || y === 0 || y === this.maxY;
  }

  isValidMove(x, y, z, history = this.history) {
    if (x > this.maxX || x < 0) {
      return false;
    }

    if (y > this.maxY || y < 0) {
      return false;
    }

    if (z > this.maxZ || z < 0) {
      return false;
    }

    const cell = history[z][y][x];

    if (cell.wall || cell.visited) {
      return false;
    }

    return true;
  }
}

module.exports = Maze;
